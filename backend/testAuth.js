'use strict';

const AWSXRay = require('aws-xray-sdk')
const https = require('https')
const jose = require('node-jose')

let region = 'us-east-1'
let userpool_id = `${region}_Zd2rjxnX9`
let app_client_id = '1so6m71ukqgp3uuj20qj2v3p0m'
let keys_url = `https://cognito-idp.${region}.amazonaws.com/${userpool_id}/.well-known/jwks.json`

const authorizedPolicy = {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "execute-api:Invoke",
            "Resource": `arn:aws:execute-api:${region}:385415781098:1lsxs7qe0c/*/GET/helloWorld`
        }
    ]
}

module.exports.testAuthorizer = (event, context, callback) => {
    let customAuthorizer = new AWSXRay.Segment('Custom Authorization')
    AWSXRay.setSegment(customAuthorizer)

    customAuthorizer.addMetadata('lambdaConfiguration', {
        region,
        userpool_id,
        app_client_id,
        keys_url
    })
    
    let jwtParser = customAuthorizer.addNewSubsegment('JWT Token Parser')

    //Begin Token Validation
    let token = event.authorizationToken
    let sections = token.split('.')
    //Get kid from headers prior to verification
    let header = jose.util.base64url.decode(sections[0])
    let headerPreParse = header

    // Catch a parsing error so Lambda function exits gracefully
    // Found to be breaking segments
    // try {
    //     header = JSON.parse(header)
    // } catch(err) {
    //     console.log(err)
    //     jwtParser.addError(new Error('Invalid token format'))
    //     jwtParser.close()
    //     customAuthorizer.close()
    //     return callback('Invalid token format')
    // }

    header = JSON.parse(header)

    let kid = header.kid
    
    let validatedClaims = null
    let authResponse = null

    jwtParser.addMetadata('parsedData', {
        token,
        sections,
        headerPreParse,
        headerPostParse: header
    })
    jwtParser.close()

    let getKeys = customAuthorizer.addNewSubsegment('Retrieve Well Known Keys')
    //Download public keys
    https.get(keys_url, response => {
        getKeys.close()
        if(response.statusCode == 200) {
            response.on('data', body => {
                let parseKeys = customAuthorizer.addNewSubsegment('Parse Well Known Keys')
                let keys = JSON.parse(body)['keys']

                parseKeys.addMetadata('keys', keys)
                parseKeys.close()

                let kidSearch = customAuthorizer.addNewSubsegment('Well Known Key Search')
                kidSearch.addMetadata('data', {
                    kid,
                    keys
                })
                //Search for the kid in the downloaded public keys
                let key_index = -1
                for(let i = 0; i < keys.length; i++) {
                    if (kid == keys[i].kid) {
                        key_index = i
                        break
                    }
                }
                
                //Key wasn't found in Cognito Pool
                if(key_index == -1) {
                    kidSearch.addError('Public key not found')
                    kidSearch.close()
                    customAuthorizer.close()
                    return callback('Public key not found')
                }

                kidSearch.close()

                let keyConstruction = customAuthorizer.addNewSubsegment('JWT Public Key Construction')
                keyConstruction.addMetadata('data', {
                    key: keys[key_index]
                })
                //Begin construction of public key
                jose.JWK.asKey(keys[key_index]).then(result => {
                    keyConstruction.close()
                    let keyVerification = customAuthorizer.addNewSubsegment('JWT Token Verification')
                    keyVerification.addMetadata('data', {result})
                    //Verify the signiture
                    jose.JWS.createVerify(result).verify(token).then(result => {
                        keyVerification.addMetadata('result', {result})
                        //Now we can use the claims
                        let claims = JSON.parse(result.payload)

                        //Verify token expiration
                        let current_ts = Math.floor(new Date() / 1000)
                        
                        //Token is expired
                        if(current_ts > claims.exp) {
                            keyVerification.addError('Token has expired')
                            keyVerification.close()
                            customAuthorizer.close()
                            return callback('Token has expired')
                        }

                        //Check if token was issued for this client_id
                        if(claims.aud != app_client_id) {
                            keyVerification.addError('Token was not issued for this audience')
                            keyVerification.close()
                            customAuthorizer.close()
                            return callback('Token was not issued for this audience')
                        }

                        //Otherwise return claims
                        validatedClaims = claims

                        //User is valid, send IAM policy
                        authResponse = {}
                        authResponse.principalId = `user|${claims.sub}`
                        authResponse.policyDocument = authorizedPolicy
                        
                        keyVerification.close()
                        customAuthorizer.close()
                        return callback(null, authResponse)
                    }).catch(()=> {
                        keyVerification.close()
                        customAuthorizer.close()
                        return callback('Signature verification failed')
                    })
                })
            })
        }
    })
}