service:
  name: quicker-bytes
  awsTracingConfig:
    mode: Active
plugins:
  - serverless-s3-sync
custom:
  s3Sync:
    - bucketName: quickerbytes-web-${self:provider.stage}
      localDir: ../frontend/dist
package:
  include:
    - hello.js
    - testAuth.js
    - zillowTest.js
  exclude:
    - package-lock.json
    - package.json
    - .git/**
provider:
  name: aws
  runtime: nodejs8.10
  stage: dev
  region: us-east-1
  iamRoleStatements:
    - Effect: Allow
      Action:
        - s3:*
      Resource: arn:aws:s3:::quickerbytes-web-${self:provider.stage}/*
    - Effect: Allow
      Action:
        - cognito-idp:*
      Resource: arn:aws:cognito-idp:${self:provider.region}:385415781098:userpool/us-east-1_Zd2rjxnX9
    - Effect: Allow
      Action:
        - dynamodb:*
      Resource: arn:aws:dynamodb:${self:provider.region}:385415781098:table/quickerbytes-food-items-${self:provider.stage}
    - Effect: Allow
      Action:
        - apigateway:POST
      Resource: arn:aws:apigateway:*::/restapis/*/authorizers
      Condition:
        ArnLike:
          apigateway:CognitoUserPoolProviderArn:
            - arn:aws:cognito-idp:${self:provider.region}:385415781098:userpool/us-east-1_Zd2rjxnX9
    - Effect: Allow
      Action:
        - apigateway:PATCH
      Resource: arn:aws:apigateway:*::/restapis/*/authorizers/*
      Condition:
        ArnLike:
          apigateway:CognitoUserPoolProviderArn:
            - arn:aws:cognito-idp:${self:provider.region}:385415781098:userpool/us-east-1_Zd2rjxnX9
    - Effect: Allow
      Action:
        - xray:PutTraceSegments
        - xray:PutTelemetryRecords
      Resource: "*"
functions:
  hello:
    handler: hello.helloWorld
    name: quickerbytes-helloWorld-${self:provider.stage}
    description: Quicker Bytes Authorizer Hello World
    memorySize: 128
    runtime: nodejs8.10
    events:
      - http:
          path: helloWorld
          method: get
          cors: true
          integration: lambda
          authorizer:
            name: testAuth
            identitySource: method.request.header.Authorization
            claims:
              - email
  testAuth:
    handler: testAuth.testAuthorizer
    name: quickerbytes-testAuth-${self:provider.stage}
    description: Quicker Bytes Authorizer Test Function
    memorySize: 128
    runtime: nodejs8.10
  testZillow:
    handler: zillowTest.zillowTest
    name: quickerbytes-zillowTest-${self:provider.stage}
    description: Test function for Zillow API
    memorySize: 128
    runtime: nodejs8.10
    events:
      - http:
          path: zillowTest
          method: get
          cors: true
resources:
  Resources:
    StaticSite:
      Type: AWS::S3::Bucket
      Properties:
        AccessControl: PublicRead
        BucketName: quickerbytes-web-${self:provider.stage}
        WebsiteConfiguration:
          IndexDocument: index.html
    StaticSiteS3BucketPolicy:
      Type: AWS::S3::BucketPolicy
      Properties:
        Bucket:
          Ref: StaticSite
        PolicyDocument:
          Statement:
            - Sid: PublicReadGetObject
              Effect: Allow
              Principal: "*"
              Action: s3:GetObject
              Resource:
                Fn::Join:
                  - ""
                  -
                    - "arn:aws:s3:::"
                    -
                      Ref: StaticSite
                    - "/*"
      DependsOn:
        - StaticSite
    StaticSiteCloudFrontDistribution:
      Type: AWS::CloudFront::Distribution
      Properties:
        DistributionConfig:
          Origins:
            - DomainName: quickerbytes-web-${self:provider.stage}.s3.amazonaws.com
              Id: QuickerBytes1
              CustomOriginConfig:
                HTTPPort: 80
                HTTPSPort: 443
                OriginProtocolPolicy: https-only
              ## In case you want to restrict the bucket access use S3OriginConfig and remove CustomOriginConfig
              # S3OriginConfig:
              #   OriginAccessIdentity: origin-access-identity/cloudfront/E127EXAMPLE51Z
          Enabled: true
          DefaultRootObject: index.html
          HttpVersion: http2
          #For Single Page App
          CustomErrorResponses:
            - ErrorCode: 404
              ResponseCode: 200
              ResponsePagePath: /index.html
          DefaultCacheBehavior:
            Compress: true
            AllowedMethods:
              - DELETE
              - GET
              - HEAD
              - OPTIONS
              - PATCH
              - POST
              - PUT
            TargetOriginId: QuickerBytes1
            ForwardedValues:
              QueryString: false
              Cookies:
                Forward: none
            ViewerProtocolPolicy: redirect-to-https
          ViewerCertificate:
            CloudFrontDefaultCertificate: true
      DependsOn:
        - StaticSite
    CognitoUserPool:
      Type: AWS::Cognito::UserPool
      Properties:
        AdminCreateUserConfig:
          AllowAdminCreateUserOnly: false
        AutoVerifiedAttributes:
          - email
        DeviceConfiguration:
          ChallengeRequiredOnNewDevice: true
          DeviceOnlyRememberedOnUserPrompt: true
        UserPoolName: quickerbytes-${self:provider.stage}
        Schema:
          - Name: email
            AttributeDataType: String
            Mutable: false
            Required: true
      DependsOn:
        - StaticSite
    CognitoUserPoolClient:
      Type: AWS::Cognito::UserPoolClient
      Properties:
        ClientName: quickerbytes-${self:provider.stage}-UserPoolClient
        UserPoolId:
          Ref: CognitoUserPool
        GenerateSecret: false
        RefreshTokenValidity: 30
        ReadAttributes:
          - email
      DependsOn:
        - CognitoUserPool
    FoodItemsTable:
      Type: AWS::DynamoDB::Table
      Properties:
        TableName: quickerbytes-food-items-${self:provider.stage}
        AttributeDefinitions:
          - AttributeName: Id
            AttributeType: S
        KeySchema:
          - AttributeName: Id
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1
      DependsOn:
        - StaticSite
    UnauthorizedRole:
      Type: AWS::IAM::Role
      Properties:
        AssumeRolePolicyDocument:
          Version: "2012-10-17"
          Statement:
            - Effect: Allow
              Principal:
                Federated: cognito-identitiy.amazonaws.com
              Action:
                - sts:AssumeRoleWithWebIdentity
              Condition:
                StringEquals:
                  cognito-identity.amazonaws.com:aud:
                    Ref: CognitoIdentityPool
                ForAnyValue:StringLike:
                  cognito-identity.amazonaws.com:amr: unauthenticated
        Policies:
          - PolicyName: "CognitoUnauthorizedPolicy"
            PolicyDocument:
              Version: "2012-10-17"
              Statement:
                - Effect: Allow
                  Action:
                    - mobileanalyics:PutEvents
                    - cognito-sync:*
                  Resource: "*"
      DependsOn:
        - CognitoIdentityPool
    AuthorizedRole:
      Type: "AWS::IAM::Role"
      Properties:
        AssumeRolePolicyDocument:
          Version: "2012-10-17"
          Statement:
            - Effect: "Allow"
              Principal:
                Federated: "cognito-identity.amazonaws.com"
              Action:
                - "sts:AssumeRoleWithWebIdentity"
              Condition:
                StringEquals:
                  "cognito-identity.amazonaws.com:aud":
                    Ref: CognitoIdentityPool
                "ForAnyValue:StringLike":
                  "cognito-identity.amazonaws.com:amr": authenticated
        Policies:
          - PolicyName: "CognitoAuthorizedPolicy"
            PolicyDocument:
              Version: "2012-10-17"
              Statement:
                - Effect: "Allow"
                  Action:
                    - "mobileanalytics:PutEvents"
                    - "cognito-sync:*"
                    - "cognito-identity:*"
                  Resource: "*"
                - Effect: "Allow"
                  Action:
                    - "lambda:InvokeFunction"
                  Resource: "*"
                - Effect: "Allow"
                  Action:
                    - "execute-api:Invoke"
                  Resource: "arn:aws:execute-api:us-east-1:385415781098:1lsxs7qe0c/*/GET/helloWorld"
      DependsOn:
        - CognitoIdentityPool
    IdentityPoolRoleMapping:
      Type: AWS::Cognito::IdentityPoolRoleAttachment
      Properties:
        IdentityPoolId:
          Ref: CognitoIdentityPool
        Roles:
          authenticated:
            'Fn::GetAtt': [ AuthorizedRole, Arn ]
          unauthenticated:
            'Fn::GetAtt': [ UnauthorizedRole, Arn ]
      DependsOn:
        - CognitoIdentityPool
        - AuthorizedRole
        - UnauthorizedRole
    CognitoIdentityPool:
      Type: AWS::Cognito::IdentityPool
      Properties:
        IdentityPoolName: quickerbytes_${self:provider.stage}
        AllowUnauthenticatedIdentities: false
        CognitoIdentityProviders:
          - ClientId:
              Ref: CognitoUserPoolClient
            ProviderName:
              'Fn::GetAtt': [ CognitoUserPool, ProviderName ]
      DependsOn:
        - CognitoUserPool
        - CognitoUserPoolClient
Outputs:
  WebAppCloudFrontDistributionOutput:
    Value:
      'Fn::GetAtt': [ WebAppCloudFrontDistribution, DomainName ]
  UserPoolId:
    Value:
      Ref: CognitoUserPool
  UserPoolClientId:
    Value:
      Ref: CognitoUserPoolClient