'use strict';

const AWSXRay = require('aws-xray-sdk')
const axios = require('axios')
const xml2js = require('xml2js')

const zillowKey = 'X1-ZWz1fvlvs8b1mz_4lr3d'
const getSearchURL = 'https://www.zillow.com/webservice/GetSearchResults.htm'

module.exports.zillowTest = async (event, context, callback) => {
    let entry = new AWSXRay.Segment('Get Search Results')
    AWSXRay.setSegment(entry)

    if(event.queryStringParameters.address == undefined || event.queryStringParameters.city ==  undefined || event.queryStringParameters.state == undefined) {
        entry.addError('Missing required parameters')
        entry.addMetadata('input', event)
        entry.addFaultFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 400, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Missing required parameters"}})
        return
    } else if (zillowKey == undefined) {
        entry.addError('Missing Zillow Key')
        entry.addFaultFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 500, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Missing Zillow API Key"}})
        return
    }

    let address = event.queryStringParameters.address
    let citystatezip = event.queryStringParameters.city + ', ' + event.queryStringParameters.state

    entry.addMetadata('configuration', {
        apiKey: zillowKey,
        requestType: 'GetSearchResults'
    })

    let houseSearch = entry.addNewSubsegment('Contacting Zillow - House Search')

    houseSearch.addMetadata('payload', {
        'zws-id': zillowKey,
        address,
        citystatezip
    })

    let res = await axios.get(getSearchURL, {
        params: {
            'zws-id': zillowKey,
            'address': address,
            'citystatezip': citystatezip
        }
    })

    houseSearch.close()

    let convertToJSON = entry.addNewSubsegment('Convert XML to JSON')

    convertToJSON.addMetadata('payload', res.data)

    let responseJSON = null
    xml2js.parseString(res.data, (err, result) => {
        if(err) {
            console.log(err)
            convertToJSON.addError('Error in converting XML')
            convertToJSON.addMetadata(res.data)
            convertToJSON.addErrorFlag()
            convertToJSON.close()
            entry.addFaultFlag()
            entry.close()
            callback({isBase64Encoded:false, statusCode: 500, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Error in converting XML"}})
            return
        } else if(result) {
            responseJSON = JSON.parse(JSON.stringify(result))
        }
    })

    let apiStatus = null
    let apiData = null
    let zestimatePreFormat = null

    try {
        //Data contains the results from the API if any
        apiStatus = responseJSON['SearchResults:searchresults'].message[0]
        apiData = responseJSON['SearchResults:searchresults'].response[0].results[0].result[0]
        zestimatePreFormat = apiData.zestimate[0]
    } catch(TypeError) {
        convertToJSON.addError('Error in parsing JSON')
        convertToJSON.addErrorFlag()
        convertToJSON.addMetadata('converted', responseJSON)
        convertToJSON.close()
        entry.addFaultFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 500, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Error in parsing JSON"}})
        return
    }

    convertToJSON.addMetadata('apiStatus', responseJSON['SearchResults:searchresults'].message[0])
    convertToJSON.addMetadata('converted', responseJSON)

    convertToJSON.close()

    let searchResVal = entry.addNewSubsegment('Response Validation - House Search')

    // Maybe make this a case switch
    if(apiStatus.code[0] == 0) console.log('Request Successful')
    else if(apiStatus.code[0] == 2) {
        searchResVal.addError(apiStatus.text[0])
        searchResVal.addErrorFlag()
        searchResVal.close()
        entry.addErrorFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 500, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Zillow API Key is invalid"}})
        return
    } else if(apiStatus.code[0] == 3) {
        searchResVal.addError(apiStatus.text[0])
        searchResVal.addErrorFlag()
        searchResVal.close()
        entry.addErrorFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 500, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Zillow is currently unavailable"}})
        return
    } else if(apiStatus.code[0] == 4) {
        searchResVal.addError(apiStatus.text[0])
        searchResVal.addErrorFlag()
        searchResVal.close()
        entry.addErrorFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 500, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Access to this Zillow resource is currently unavailable"}})
        return
    } else if(apiStatus.code[0] == 505) {
        searchResVal.addError(apiStatus.text[0])
        searchResVal.addErrorFlag()
        searchResVal.close()
        entry.addErrorFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 500, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Zillow timed out"}})
        return
    } else if(apiStatus.code[0] == 500) {
        searchResVal.addError(apiStatus.text[0])
        searchResVal.addErrorFlag()
        searchResVal.close()
        entry.addErrorFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 400, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Address is invalid"}})
        return
    } else if(apiStatus.code[0] == 501) {
        searchResVal.addError(apiStatus.text[0])
        searchResVal.addErrorFlag()
        searchResVal.close()
        entry.addErrorFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 400, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "City, State, and/or Zip is invalid"}})
        return
    } else if(apiStatus.code[0] == 502) {
        searchResVal.addError(apiStatus.text[0])
        searchResVal.addErrorFlag()
        searchResVal.close()
        entry.addErrorFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 404, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Not found"}})
        return
    } else if(apiStatus.code[0] == 503) {
        searchResVal.addError(apiStatus.text[0])
        searchResVal.addErrorFlag()
        searchResVal.close()
        entry.addErrorFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 400, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: apiStatus.text[0]}})
        return
    } else if(apiStatus.code[0] == 504) {
        searchResVal.addError(apiStatus.text[0])
        searchResVal.addErrorFlag()
        searchResVal.close()
        entry.addErrorFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 404, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Not found"}})
        return
    } else if(apiStatus.code[0] == 505) {
        searchResVal.addError(apiStatus.text[0])
        searchResVal.addErrorFlag()
        searchResVal.close()
        entry.addErrorFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 500, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Request timed out"}})
        return
    } else if(apiStatus.code[0] == 506) {
        searchResVal.addError(apiStatus.text[0])
        searchResVal.addErrorFlag()
        searchResVal.close()
        entry.addErrorFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 400, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Validation: Address too long"}})
        return
    } else if(apiStatus.code[0] == 507) {
        searchResVal.addError(apiStatus.text[0])
        searchResVal.addErrorFlag()
        searchResVal.close()
        entry.addErrorFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 400, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Validation: Address is invalid"}})
        return
    } else if(apiStatus.code[0] == 508) {
        searchResVal.addError(apiStatus.text[0])
        searchResVal.addErrorFlag()
        searchResVal.close()
        entry.addErrorFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 404, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Validation: Multiple results for inputted address"}})
        return
    } else {
        searchResVal.addError(apiStatus.text[0])
        searchResVal.addErrorFlag()
        searchResVal.close()
        entry.addErrorFlag()
        entry.close()
        callback({isBase64Encoded:false, statusCode: 400, headers: {"Access-Control-Allow-Origin": "*"}, body: {message: "Unknown server error"}})
        return
    }

    searchResVal.close()

    let zestimateFormatting = entry.addNewSubsegment('Zestimate JSON Pretty Printing')
    zestimateFormatting.addMetadata('rawZestimate', zestimatePreFormat)
    //Done because XML to JSON adds arrays to every object reference
    //Maybe look for a library or make one to automatically strip these arrays
    let zestimate = {
        amount: {
            price: zestimatePreFormat.amount[0]._,
            currency: zestimatePreFormat.amount[0].$.currency
        },
        "last-updated": zestimatePreFormat['last-updated'][0],
        oneWeekChange: {
            deprecated: zestimatePreFormat.oneWeekChange[0].$.deprecated
        },
        valueChange: {
            amount: zestimatePreFormat.valueChange[0]._,
            duration: zestimatePreFormat.valueChange[0].$.duration,
            currency: zestimatePreFormat.valueChange[0].$.currency
        },
        valuationRange: {
            low: {
                amount: zestimatePreFormat.valuationRange[0].low[0]._,
                currency: zestimatePreFormat.valuationRange[0].low[0].$.currency
            },
            high: {
                amount: zestimatePreFormat.valuationRange[0].high[0]._,
                currency: zestimatePreFormat.valuationRange[0].high[0].$.currency
            }
        },
        percentile: zestimatePreFormat.percentile[0]
    }

    let gatewayResponse = {
        isBase64Encoded: false,
        statusCode: 200,
        headers: { 
            "Access-Control-Allow-Origin": "*" 
        },
        body: JSON.stringify(zestimate)
    }

    callback(null, gatewayResponse)

    zestimateFormatting.close()

    entry.close()
}