'use strict';

module.exports.helloWorld = (event, context, callback) => {
  console.log('Function invoked')
  console.log(event)
  console.log(context)
  callback(null, {
    statusCode: 200,
    body: JSON.stringify({ message: 'Hello world!' })
  })
}